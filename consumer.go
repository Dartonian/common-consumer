package consumer

import (
	kafka "github.com/segmentio/kafka-go"
)

// New kafka reader
func New(URL, topic string) *kafka.Reader {
	return kafka.NewReader(kafka.ReaderConfig{
		Brokers:  []string{URL},
		Topic:    topic,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})
}
